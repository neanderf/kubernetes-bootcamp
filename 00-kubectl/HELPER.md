# Common Commands

This is a set of common commands used on `kubectl` and `gcloud` for GKE


## Switching Clusters

### 1. gcloud configuration

Set the current configuration of gcloud to YOUR project.  This series of commands are used to create a configuration.  Switching a configuration from a previously created configuraiton is listed second.

```bash
gcloud config configurations create <SOME-UNIQUE-NAME>
gcloud auth login # will open a browser
gcloud config set project <PROJECT-NAME>
```

#### Switch `gcloud` Configurations

```bash
# See what configurations are available
gcloud config configurations list

#OPTIONALLY
gcloud config configurations delete <name-of-something>

gcloud config configurations activate <name-of-desired-config>
```

### 2. GKE Configuration (set context)

```bash
# gcloud configuration MUST be in context
gcloud container clusters get-credentials <cluster-name> --zone=us-west1-a

# list current context
kubectl config current-context

# use namespace
kubectl config set-context $(kubectl config current-context) --namespace=#########
```

## Switching Contexts

This section will show how to switch contexts between Kubernetes clusters (even across projects)

### Current Context

Display the current context name

```bash 
kubectl config current-context
```

### Rename the current context

```bash
# Rename current context
kubectl config rename-context $(kubectl config current-context) <new name>
```

### Switch kubectl contexts

```bash
# switch context
kubectl config use-context <context-name>
```

## Helpful `kubectl` commands

The following CLI commands are useful in different contexts

### Flags

|  Flag    | Options  | Comment
|:--------------|---| ---------------------|
| --record=true | True/**False** | Records commands for `rollout` reversal |
|  -o [output] | wide/yaml/json/jsonpath | Changes the output of the response for all "get" requests (ie, `kubectl get pod ...`, `kubectl get deployment ...`) |
|  --show-labels | n/a | Displays any labels for the requested resources |

> NOTE: Bold options are default (when possible)

> Learn `jq` to use with `--jsonpath` : [download/install jq](https://stedolan.github.io/jq/download/)


### Add Ingress Controllers to `get all`

```bash
# In .profile, .bashrc, .bash_profile or .bash_aliases
alias k8-all='kubectl get all && echo  && kubectl get ingress'
```

### Proxy localhost to Cluster

```bash
kubectl proxy --port=8080
```

### Forward a deployment to localhost
```bash
kubectl port-forward deployment/simple-deployment 8081:8080
```