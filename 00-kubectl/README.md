# Overview

This file is intended to have snippts and other contextually available commands or tactics to solve the activities for the section,

## Goals
* Become familar with the `kubectl` command line tool
* Understand how to "get" resources and filter by labels

## Steps

1. Authenticating `kubectl` - Create a Kuberntes configuration (typically stored in `~/.kube/config`)
    * `gcloud container clusters get-credentials <CLUSTER_NAME> --zone <GCP_ZONE> --project <GCP_PROJECT_CONTAINING_CLUSTER>`
    * Note: If using Regional clustes, the "zone" changes, refer to GCP console for templated access
    * If using Linux or Mac, install `kubectx` - https://github.com/ahmetb/kubectx
1. Getting comfortable with `kubectl`
  1. `kubectl cluster-info`
  1. Get all pods in "kube-system"
        * `kubctl get pods -n kube-system`
        * Show "wide" output
            - `kubctl get pods -n kube-system -o wide`
        * Filter returned pods by "tier=node"
  1. Describe one of those pods
        * `kubectl describe <pod-name>`

## Done

* Commands given to `kubectl` respond with out exceptions
* Able to list and describe pods in `kube-system` namespace

## Navigation
[Home](../) | [Namespaces](../01-namespaces "01-namespaces lab") | [I need help!!](HELPER.md)

---

### Annotations vs Labels

* Annotations
  * Used to hold specific metadata on resource and are not intended to be used for identifying resources (ie, no search)
  * NOT indexed or easily searchable
  * Use annotation-namespace (Kubernetes namespace `*.kubernetes.io/<var-name>` - ex: `kubernetes.io/hostname=amd64`)
* Labels
  * Used as selector and  are intended to be used for identifying resources
    * key/value pair
  * Indexed and searchable
    * `kubectl get <resoruce> -l <label-pair>`

## Get Resources

### Pods

Use "pod", "pods" or "po"

```bash
# get long notation
kubectl get pod

# short notation
kubectl get po
```

### Get All Resources (nearly all)

```bash
kubectl get all
```

### Get Resoruces By Labels

```bash
kubectl get <RESOURCE_TYPE> -l label_key=label_value

kubectl get pods -l app=nginx

kubectl get pods -l 'label in (multiple,values,possible)'
```

## Describe

Ability to see detailed information on the resource

### Describe a resource

```bash
kubectl describe <resource>
```
