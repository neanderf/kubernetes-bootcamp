# Overview

This lab will describe the uses of "namespaces" for Kubernetes

## Goals

* List all namespaces
* **Create a namespace**
* Set config context to your namespace by default
* Describe resources, labels and annotations in your namespace
* Set default resource limits for a namespace

## Exercises

1. List all pods in ALL NAMESPACES
1. List all namespaces
1. Create your personal namespace
1. Switch context to your namespace
1. See if there are any resource limits in your namespace
1. Add default CPU & RAM limits for your namespace
1. Add ResourceQuota limits (LimitRange)

## Done

* Listing out all namespaces includes the created namespace
* kubectl context is set to crated namespace

## Navigation
[kubectl](../00-kubectl) | [Pods](../02-pods "02-pods lab") | [I need help!!](HELPER.md)

---

## Making Changes

Changes are conducted using descriptor files (yaml) that describe the desired state (declarative).

### Commands

```bash
# Apply a file to Kubernetes
kubectl apply -f <filename>

# Delete a file contents
kubectl delete -f <filename>

```

## Useful Notes

* `apply` vs `create` = Declarative vs. imperative  (where possible, always use `apply`)
* Pods requesting more resources than `Node` or `LimitRange` capabilities will **NOT** be scheduled (applies to request, limits are still scheduled, but capped at the LimitRange or Node capacity)

