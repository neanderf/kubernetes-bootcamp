# Overview

This file is intended to have snippts and other contextually available commands or tactics to solve the activities for the section

### Run a pod

```bash
# Run the latest nginx
kubectl run nginx-simple --image=nginx --port=80
```

### SSH into pod
```bash
kubectl exec -it <NAME OF POD FROM OUTPUT> -- /bin/bash

--- example output ---

root@nginx-simple-cc84c6766-xqb94:/# ls -al
total 76
drwxr-xr-x   1 root root 4096 May 19 02:49 .
drwxr-xr-x   1 root root 4096 May 19 02:49 ..
-rwxr-xr-x   1 root root    0 May 19 02:49 .dockerenv
drwxr-xr-x   2 root root 4096 Apr 26 00:00 bin
drwxr-xr-x   2 root root 4096 Feb 23 23:23 boot
drwxr-xr-x   5 root root  360 May 19 02:49 dev
drwxr-xr-x   1 root root 4096 May 19 02:49 etc
drwxr-xr-x   2 root root 4096 Feb 23 23:23 home
drwxr-xr-x   1 root root 4096 Apr 26 00:00 lib
drwxr-xr-x   2 root root 4096 Apr 26 00:00 lib64
drwxr-xr-x   2 root root 4096 Apr 26 00:00 media
drwxr-xr-x   2 root root 4096 Apr 26 00:00 mnt
drwxr-xr-x   2 root root 4096 Apr 26 00:00 opt
dr-xr-xr-x 108 root root    0 May 19 02:49 proc
drwx------   2 root root 4096 Apr 26 00:00 root
drwxr-xr-x   1 root root 4096 May 19 02:49 run
drwxr-xr-x   2 root root 4096 Apr 26 00:00 sbin
drwxr-xr-x   2 root root 4096 Apr 26 00:00 srv
dr-xr-xr-x  12 root root    0 May 19 00:02 sys
drwxrwxrwt   1 root root 4096 Apr 30 13:55 tmp
drwxr-xr-x   1 root root 4096 Apr 26 00:00 usr
drwxr-xr-x   1 root root 4096 Apr 26 00:00 var

root@nginx-simple-cc84c6766-xqb94:/# exit
exit
```

### Delete a non-yaml run pod

```bash
# Delete a NON-Descriptor Pod (Deployments)
kubectl delete deployment nginx-simple
```

### Create a Pod with YAML

```bash
kubectl apply -f 02-pods/pod-single-example.yaml
```

### Delete a Pod with YAML

```bash
kubectl delete -f 02-pods/pod-single-example.yaml
```

### Read a log

```bash
# Definition
kubectl logs -f <podname> <container-name-if-multi-container>

# Example
kubectl logs -f nginx-multi busybox
```

### Add Resource Limits & Requests
```yaml
    # inside of a Pod or Spec/Template
    #...
    image: <container image name + optional label>
    name: <container-name>
    resources:
        limits:
            memory: 512Mi
        requests:
            memory: 256Mi
    #...
```

## Best Practices

* If you have a long shutdown period, it is good to define a `terminationGracePeriodSeconds` field to give your container the required time to shutdown before receiving a SIGKILL (Default is 30s)

* Define resource limits for pods
  * Often horizontal scale is preferred over vertical scale (smaller CPU and RAM with more instances)
  * Difference between `limits` and `requests`
  * **512Mi** is the default namespace limit
