# Overview

This file is intended to have snippts and other contextually available commands or tactics to solve the activities for the section

### Update ReplicaSet Image Version

```bash
# set new image
kubectl set image deployment/simple-deployment nginx=nginx:1.12-alpine --record=true
```

**NOTE:  `--record=true` allows rollout history to record command**

### Rollout History
```bash
kubectl rollout history deployment/nginx
```

### Rollback
```bash
kubectl rollout undo deployment simple-deployment --to-revision=1
```

### Create new deployment

```bash
# Create from a deployment manifest descriptor
kubectl apply -f 03-deployments/deployment-example.yaml
```

### Update deployments

NOTE: Updating the descriptor file and using `apply` is the preferred method.  The below example does not necessarily need to use a new file, the original file can be updated and applied with the same result.

```bash
# Create a second deployment (copy with desired modifications) manifest descriptor
kubectl apply -f 03-deployments/deployment-update-example.yaml
```

### Scale a deployment

```bash
# Scale to 3 replicas with modification of deployment manifest (option 1)
kubectl apply -f 03-deployments/deployment-example.yaml

# Scale to 3 replicas scale (option 2, tyically only used when orig deployment files not available)

kubectl scale --replicas=3 deployment simple-deployment
```

### Delete a deployment

```bash
# From a deployment manifest descriptor
kubectl delete -f 03-deployments/deployment-example.yaml
```
