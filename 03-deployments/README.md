# Deployments

## Goals

* Create a simple deployment using a manifest descriptor file
* Setup multiple replicas
* Understand RollingUpdate configuration specifications
* Perform a simple `set-image` with older version of the `nginx` image
* See history of changes to Deployment
* Rollback from history to previous Deployment
* Perform a scale `replicas` update with updated deployment descriptor
    * Try changing scale up and down
* Delete deployment

## Done
* Deployments create pods that can be queried/retrieved using `kubectl get po`
* No deployments in namespace at end of lab
* Practice updating and rolling back deployment

## Navigation
[Pods](../02-pods) | [Services](../04-services "04-services lab") | [I need help!!](HELPER.md)

---

### NOTE

* `rolling-update` keyword is legacy for `ReplicationControllers`
    * RollingUpdate strategy is default for `set image` updates on Deployments
* `patch` is preferred over `set-image` as it has larger potential scope changes
* Using `apply -f <filename>` is preferred over all options

## Exercises

1. Create a simple, single replica `deployment`
1. Perform a `set-image` replacement of `nginx:latest` with `nginx:1.12-alpine` (NOTE: Don't forget `--record=true`)
    * Describe deployment to ensure image change was applied
    * Watch the "surge" of containers as `rolling update` is applied
    * Describe one of the Pods to see `nginx:1.12-alpine` image version used
1. Rollback deployment
    * Describe one of the new pods to see `nginx:latest` image version used
1. Update (via `apply`) the deployment to `nginx:1.12-alpine`
    * Perform `describe` on a pod to ensure image is 1.12-alpine version
1. Adjust the Deployment back to `nginx:latest` and update (via 'apply') the deployment
    * Perform `describe` on a pod to ensure image is `nginx:latest`
1. Scale the replica set to 3 instances (using `apply` or `scale`)
    * `kubectl get all` to demonstrate additional pods
    * (optional) update the deployment to 2 replicas, what happens?
1. Delete the deployment
