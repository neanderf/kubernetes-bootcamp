# Overview

This file is intended to have snippts and other contextually available commands or tactics to solve the activities for the section.

## Determine endpoints for Service

Prints the endpoints that are available for the service (ie. Pods that are associated to the Service)

```bash
kubectl get endpoints <service-name>
```

## Detailed description of Service
```bash
kubectl describe services <service-name>
```

## Navigate to service via Proxy

1. Start the proxy
    ```bash
    kubectl proxy --port=8080
    ```
1. Browse to the Proxy/Service URL (See Browser Access to Service below)

**NOTE: Proxy method is NOT recommended for production use!!!**

### Browser access to Service (NodePort and ClusterIP only)

`http://localhost:8080/api/v1/namespaces/<NAMESPACE>/services/<SERVICE-NAME>:<PORT-NAME>/proxy/`

| Variable                    | Example Value      |
| --------------------------- |:------------------ |
| NAMESPACE                   | nortal-namespace   |
| SERVICE-NAME                | named-service      |
| SERVICE-PORT-NAME           | http               |

```bash
# Example
http://localhost:8081/api/v1/namespaces/mikes-namespace/services/nginx-service-cluster:http/proxy/
```

```bash
# Port Forwarding

kubectl port-forward svc/nginx-service-cluster 8080:80
```