# <NAEME>

## Goals

* Create an `ingress` controller for exposes service

### NOTE

* TLS can be defined when combining a `secret` containing the key & and cert (base64 format)
* Host and Path routing possible with configuration
* Handle multiple services
* Defaults to Google Global Load Balancer in GKE (optional installation of nginx/nginx plus, trafek, others)

> Ingress controllers use GLBs and can take 3-5 minutes to create

## Exercises

1. Create a new ingress controller for service created in 05-configuration (service = simple-echo-service-np)
