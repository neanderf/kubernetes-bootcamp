# Jobs and Workloads

## Goals

* Understand difference between Jobs and CronJobs
* Build a Job
* Manage lifecycle of a CronJob

### NOTE

* <HELPFUL NOTES>
* **Not covering Job Queues**

## Exercises

1. Build and execute a Job
1. View and Remove Job after completion
1. Build and execute a CronJob
1. View history of a CronJob
1. Remove a CronJob
