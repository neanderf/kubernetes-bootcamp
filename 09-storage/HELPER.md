# Overview

This file is intended to have snippts and other contextually available commands or tactics to solve the activities for the section

## Commands

### SOME COMMAND
```bash
kubectl ...
```

        ## on env/cmd/name level
        volumeMounts:
        - mountPath: /app/config/file.yml
          name: mams-migrate
          subPath: file.yml
      ## On Container Level
      volumes:
      - name: app-etl
        configMap:
          name: etl
