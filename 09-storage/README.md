# Storage

## Goals

* Create a Storage Class
* Undestand a Persistant Volume Claim
* Link PVC with Storage Class to Deployment

### NOTE

* Storage Classes define the type of block storage to be used by a Persistant Volume Claim (PVC)
    * SSD, RAMDISK, standard, etc
* Persistant Volume Claim provisions the storage and makes avaiable for `VolumeMount` on containers in Deployments


## Exercises

1. Create a Storage Class
1. Create a PVC
1. Udpate a deployment to mount volume of PVC

## Examples

### StorageClass (Google)
```yaml
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: standard-storage
provisioner: kubernetes.io/gce-pd
parameters:
  type: pd-standard
  replication-type: regional-pd # Powerful w/StatefulSets+Regional clusters
volumeBindingMode: WaitForFirstConsumer
allowedTopologies:
- matchLabelExpressions:
  - key: failure-domain.beta.kubernetes.io/zone
    values:
    - europe-central1-a
    - europe-central1-b
```

### Persistent Volume Claim
```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: long-term-storage
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 8Gi
  storageClassName: standard-storage
```

### Mounting
```yaml
apiVersion: apps/v1beta2
kind: Deployment
metadata:
  name: my-deployment
spec:
  selector:
    matchLabels:
      app: appv1
  replicas: 1
  template:
    metadata:
      labels:
        app: appv1
    spec:
      containers:
        image: "SOME/IMAGE:1.1.1"
        volumeMounts:
        - mountPath: /data
          name: lts-file-data
      volumes:
      - name: lts-file-data
        persistentVolumeClaim:
          claimName: long-term-storage
```
