# Overview

* Who is Dev9?
* What are we doing?
* How to participate?

## Agenda

### Primary Bootcamp

|   Time   | Duration | Topic                                            |
|:---------|:--------:|:-------------------------------------------------|
| 09:30am  |    15m   | Goals/Objectives & Workshop Directions           |
| 09:45am  |    15m   | Kubernetes Overview & Environment Validation     |
| 10:00am  |    1h    | Pods & Containers                                |
| 11:00am  |    1h    | Deployments & Configuration                      |
| 12:00pm  |    1h    | **Lunch**                                        |
| 01:00pm  |    1h    | Services                                         |
| 02:00pm  |    30m   | Ingress Controllers                              |
| 02:30pm  |    30m   | Jobs & CronJobs                                  |
| 03:00pm  |    30m   | Secrets & Storage                                |
| 03:30pm  |    30m   | Scaling (Node & HorizontalPodScaler)             |
| 04:00pm  |    TBD   | Questions & Answers                              |
