provider "google" {
  # This is the SA for Terraform
  credentials = "${file(local.credentials_file_path)}"
  project     = "${var.project}"
  region      = "${var.region}"
}

provider "random" {
  version = "~> 1.1"
}

locals {
  credentials_file_path = "$var.credentials"
  temp_project_id        = "${var.random_project_id ? format("%s-%s",var.project,random_id.random_project_id_suffix.hex) : var.project}"
}

### NOTE:  Backend Configuration is required due to interpollation created during
### the core service and backends are created before
### https://www.terraform.io/docs/backends/config.html
terraform {
  backend "gcs" {
    # credentials = ## Use CLI: -backend-config="credentials=/..."
    # bucket      = ## Use CLI: -backend-config="bucket=<somebucket>"
    # prefix      = ## Use CLI: -backend-config="prefix=terraform/state"
    ## Example:
# terraform init -backend-config="credentials=/home/user/.config/gcloud/terraform-service-account.json" \
#       -backend-config="bucket=terraform_state" \
#       -backend-config="prefix=terraform/state"

  }
}

resource "google_folder" "containing_folder" {
  display_name = "${var.folder_name}"
  parent       = "organizations/${var.organization_id}"
}


module "project-factory" {
  source            = "github.com/terraform-google-modules/terraform-google-project-factory?ref=v0.2.0"
  random_project_id = "true"
  name              = "${var.project}"
  org_id            = "${var.organization_id}"
  billing_account   = "${var.billing_account}"
  folder_id         = "${google_folder.containing_folder.id}"
  credentials_path  = "${local.credentials_file_path}"
}