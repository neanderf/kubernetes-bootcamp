### Global variables

variable "credentials" {
  # NOTE: if using $(file), must be defined in main, not in variables.tfvars for some reason
  default = "/no-credentials/here"
}

variable "project" {
  type        = "string"
  default     = "kubernetes-bootcamp"
  description = "GCP project name:  Note, this can and should be an input from the output of modules/project"
}

variable "region" {
  type        = "string"
  default     = "us-west1"
  description = "Region to deploy project into"
}

### export TF_VAR_billing_account=${GOOGLE_BILLING_ID}

variable billing_account {
  type        = "string"
  description = "Billing account for project (STORE IN TF_VAR_billing_account)"
}

### export TF_VAR_org_id=${GOOGLE_ORG_ID}
variable org_id {
  type        = "string"
  description = "Organization ID (STORE IN TF_VAR_org_id)"
}

variable "state_bucket" {
  type = "string"
  description = "Bucket containing the state file tracking. This is setup during the 'initial-setup.sh' script"
}

variable "state_bucket_prefix" {
  type = "string"
  default = "terraform/state"  
  description = "Prefix for statefile used within `state_bucket`"
}


### Required variables for Project 

variable "additional_apis" {
  type    = "list"
  default = []
}

variable "folder_name" {
  type        = "string"
  description = "Name of the folder"
}

variable "owner_email" {
  type        = "string"
  description = "Human owner of project folder"
}
